#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui>
#include <QtNetwork>
#include "xsound.h"

#define FIVE_NOP 0
#define FIVE_ACCEPT 1
#define FIVE_REFUSE 2
#define FIVE_GO 3
#define FIVE_NEWGAME 4
#define FIVE_TALK 5

#define FIVE_NONE 0
#define FIVE_BLACK 1
#define FIVE_WHITE (-1)

namespace Ui {
    class MainWindow;
}

enum GameState
{UNREADY, IN_PROGRESS, FINISHED};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void initGame();

protected:
    void paintEvent(QPaintEvent *e);
    void mousePressEvent(QMouseEvent *e);
    void sendData(QTcpSocket *s, int op, int row = -1, int column = -1);
    bool say(QString str);
    bool win(int row, int column);

private slots:
    void on_action_Exit_triggered();
    void on_action_About_triggered();
    void on_action_Port_triggered();
    void on_action_Connect_triggered();
    void on_action_NewGame_triggered();
    void on_lineEdit_returnPressed();
    void on_action_Clear_triggered();
    void newConnection();
    void readyRead();

private:
    Ui::MainWindow *ui;
    QTcpServer *tcpServer;
    QTcpSocket *tcpSocket;
    int gridSize;
    int margin;
    int timerID;
    enum GameState gameState;
    int board[15][15];
    int me; //己方的颜色(黑或者白)
    bool isMyTurn;
    QImage blackChess;
    QImage whiteChess;
    QImage blockImage;
    int lastestRow; //最近一次下棋位置
    int lastestColumn;
    XSound *sound;
};

#endif // MAINWINDOW_H
