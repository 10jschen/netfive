#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    gridSize(40),
    margin(30)
{
    ui->setupUi(this);

    blackChess.load(":/black.png");
    whiteChess.load(":/white.png");
    blockImage.load(":/block.png");
    gameState = UNREADY;

    tcpSocket = 0;
    tcpServer = new QTcpServer(this);

    connect(tcpServer, SIGNAL(newConnection()), this, SLOT(newConnection()));
    if(!tcpServer->listen(QHostAddress::Any, 8500))
    {
        QMessageBox::critical(this, "错误", tcpServer->errorString());
    }
    setFixedSize(gridSize * 14 + margin * 2 + 300, gridSize * 14 + margin * 2 + menuBar()->height());
    ui->label->move(gridSize * 14 + margin + 20, margin);
    ui->textEdit->move(gridSize * 14 + margin + 20, margin + ui->label->height());
    ui->textEdit->resize(this->width() - ui->textEdit->x() - margin, gridSize * 8);
    ui->lineEdit->move(ui->textEdit->x(), ui->textEdit->y() + ui->textEdit->height() + 10);
    ui->lineEdit->resize(ui->textEdit->width(), ui->lineEdit->height());
    ui->lineEdit->setFocus();

    initGame();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::initGame()
{
    int row, column;
    lastestRow = lastestColumn = -1;
    for(row = 0; row < 15; ++row)
        for(column = 0; column < 15; ++column)
            board[row][column] = 0;
    gameState = UNREADY;
    isMyTurn = false;
    this->repaint();
}

void MainWindow::sendData(QTcpSocket *s, int op, int row, int column)
{
    if(s && s->state() == QTcpSocket::ConnectedState)
    {
        QByteArray bytes;
        QDataStream out(&bytes, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_4_0);
        out << (quint8)op;
        if(op == FIVE_GO)
            out << (quint16)row << (quint16)column;
        s->write(bytes);
    }
}

bool MainWindow::say(QString str)
{
    if(tcpSocket && tcpSocket->state() == QTcpSocket::ConnectedState)
    {
        QByteArray bytes;
        QDataStream out(&bytes, QIODevice::WriteOnly);
        out.setVersion(QDataStream::Qt_4_0);
        out << (quint8)FIVE_TALK << (quint32)0 << str;
        out.device()->seek(sizeof(quint8));
        out << (quint32)(bytes.length() - sizeof(quint8) - sizeof(quint32));
        tcpSocket->write(bytes);
        return true;
    }
    return false;
}

void MainWindow::newConnection()
{
    QTcpSocket *s;
    s = tcpServer->nextPendingConnection();
    if(QMessageBox::question(this, "询问", QString("%1:%2 想要和你对战，是否接受挑战？").arg(
                                 s->peerAddress().toString()).arg(s->peerPort()),
                             QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
    {
        connect(s, SIGNAL(disconnected()), s, SLOT(deleteLater()));
        sendData(s, FIVE_REFUSE);
        s->disconnectFromHost();
        return;
    }

    initGame();
    me = FIVE_WHITE; //白方
    this->setStatusTip("我方乃白方");
    isMyTurn = false;
    gameState = IN_PROGRESS;
    if(tcpSocket)
    {
        tcpSocket->abort();
        tcpSocket->deleteLater();
    }
    tcpSocket = s;
    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readyRead()));
    sendData(tcpSocket, FIVE_ACCEPT);
}

void MainWindow::readyRead()
{
    quint8 op;
    quint16 row, column;
    quint32 len;
    QString str;
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_0);
    in >> op;
    switch(op)
    {
    case FIVE_NOP: /* 接收到了心跳包 */
        break;
    case FIVE_ACCEPT:
        initGame();
        me = FIVE_BLACK; //黑方
        gameState = IN_PROGRESS;
        isMyTurn = true;
        this->setStatusTip("我方乃黑方");
        QMessageBox::information(this, "提示", "对方同意，现在您是黑方，请下棋！");
        break;
    case FIVE_REFUSE:
        QMessageBox::information(this, "提示", "您的请求遭到对方拒绝");
        break;
    case FIVE_GO:
        while(tcpSocket->bytesAvailable() < (int)sizeof(quint16) * 2)
            tcpSocket->waitForReadyRead();
        in >> row >> column;
        if(row >= 0 && row < 15 && column >= 0 && column < 15 && gameState == IN_PROGRESS)
        {
            lastestRow = row;
            lastestColumn = column;
            board[row][column] = -this->me;
            isMyTurn = true;
            this->repaint();
            if(win(row, column))
            {
                gameState = FINISHED;
                QMessageBox::information(this, "游戏结束", "很遗憾，您输了。");
            }
        }
        break;
    case FIVE_NEWGAME:
        if(QMessageBox::question(this, "询问", QString("%1:%2 想要重新开局，是否同意？").arg(
                                     tcpSocket->peerAddress().toString()).arg(tcpSocket->peerPort()),
                                 QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
            sendData(tcpSocket, FIVE_REFUSE);
        else
        {
            sendData(tcpSocket, FIVE_ACCEPT);
            initGame();
            me = FIVE_WHITE;
            this->setStatusTip("我方乃白方");
            isMyTurn = false;
            gameState = IN_PROGRESS;
        }
        break;
    case FIVE_TALK:
        while(tcpSocket->bytesAvailable() < (int)sizeof(quint32))
            tcpSocket->waitForReadyRead();
        in >> len;
        while(tcpSocket->bytesAvailable() < len)
            tcpSocket->waitForReadyRead();
        in >> str;
        ui->textEdit->append(QString("对方：") + str);
        sound = new XSound(this);
        connect(sound, SIGNAL(finished()), sound, SLOT(deleteLater()));
        sound->play(":/newmessage.wav");
        break;
    }
}

bool MainWindow::win(int row, int column)
{
    int step[4][2] = {{0, 1}, {1, 0}, {1, 1}, {1, -1}};
    for(int i = 0; i < 4; ++i)
    {
        int r, c;
        int count = 1;
        r = row + step[i][0];
        c = column + step[i][1];
        while(r >= 0 && r < 15 && c >= 0 && c < 15
              && board[r][c] == board[row][column])
        {
            r += step[i][0];
            c += step[i][1];
            ++count;
        }

        r = row - step[i][0];
        c = column - step[i][1];
        while(r >= 0 && r < 15 && c >= 0 && c < 15
              && board[r][c] == board[row][column])
        {
            r -= step[i][0];
            c -= step[i][1];
            ++count;
        }

        if(count >= 5)
            return true;
    }
    return false;
}

void MainWindow::mousePressEvent(QMouseEvent *e)
{
    if(e->button() == Qt::LeftButton && gameState == IN_PROGRESS && isMyTurn)
    {
        int row, column;
        row = (e->y() - menuBar()->height() - margin + gridSize / 2) / gridSize;
        column = (e->x() - margin + gridSize / 2) / gridSize;
        if(row >= 0 && row < 15 && column >= 0 && column < 15
                && board[row][column] == FIVE_NONE)
        {
            lastestRow = row;
            lastestColumn = column;
            board[row][column] = this->me;
            isMyTurn = false;
            sendData(tcpSocket, FIVE_GO, row, column);
            this->repaint();
            if(win(row, column))
            {
                gameState = FINISHED;
                QMessageBox::information(this, "游戏结束", "恭喜，您赢了！");
            }
        }
    }
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    for(int i = 0; i < 15; ++i)
    {
        painter.drawLine(margin, margin + menuBar()->height() + i * gridSize,
                         margin + gridSize * 14, margin + menuBar()->height() + i * gridSize);
        painter.drawLine(margin + i * gridSize, margin + menuBar()->height(),
                         margin + i * gridSize, margin + menuBar()->height() + gridSize * 14);
    }
    for(int row = 0; row < 15; ++row)
        for(int column = 0; column < 15; ++column)
        {
            if(board[row][column] == FIVE_BLACK)
                painter.drawImage(margin + column * gridSize - blackChess.width() / 2,
                                  margin + menuBar()->height() + row * gridSize - blackChess.height() / 2,
                                  blackChess);
            else if(board[row][column] == FIVE_WHITE)
                painter.drawImage(margin + column * gridSize - whiteChess.width() / 2,
                                  margin + menuBar()->height() + row * gridSize - whiteChess.height() / 2,
                                  whiteChess);
        }
    if(lastestRow >= 0 && lastestColumn >= 0)
    {
        painter.drawImage(margin + lastestColumn * gridSize - blockImage.width() / 2,
                         margin + menuBar()->height() + lastestRow * gridSize - blockImage.height() / 2,
                         blockImage);
    }
}

void MainWindow::on_action_Exit_triggered()
{
    this->close();
}

void MainWindow::on_action_About_triggered()
{
    QMessageBox::about(this, "网络版五子棋", "本游戏由 Ray Fung 开发，用于教学目的。");
}

void MainWindow::on_action_Port_triggered()
{
    bool ok;
    int port;
    port = QInputDialog::getInt(this, "设置端口", "端口：", tcpServer->serverPort(), 2000, 30000, 1, &ok);
    if(ok)
    {
        tcpServer->close();
        if(tcpServer->listen(QHostAddress::Any, port))
            QMessageBox::information(this, "提示", "端口设置成功！");
        else
            QMessageBox::critical(this, "错误", tcpServer->errorString());
    }
}

void MainWindow::on_action_Connect_triggered()
{
    QString ip;
    int port;
    bool ok;
    ip = QInputDialog::getText(this, "连接到", "对方的IP地址", QLineEdit::Normal, "127.0.0.1", &ok);
    if(ok)
    {
        port = QInputDialog::getInt(this, "端口", "对方的端口号：", 8500, 2000, 30000, 1, &ok);
        if(ok)
        {
            if(tcpSocket)
            {
                tcpSocket->abort();
                tcpSocket->deleteLater();
            }
            tcpSocket = new QTcpSocket(this);
            connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readyRead()));
            tcpSocket->connectToHost(ip, port);
            this->setStatusTip("正在等待对方回复");
            if(!tcpSocket->waitForConnected(10000))
                QMessageBox::information(this, "提示", "连接失败！");
        }
    }
}

void MainWindow::on_action_NewGame_triggered()
{
    sendData(tcpSocket, FIVE_NEWGAME);
}

void MainWindow::on_lineEdit_returnPressed()
{
    if(say(ui->lineEdit->text()))
    {
        ui->textEdit->append(QString("我：") + ui->lineEdit->text());
        ui->lineEdit->clear();
    }
    else
        QMessageBox::information(this, "提示", "尚未连接到任何玩家！");
}

void MainWindow::on_action_Clear_triggered()
{
    ui->textEdit->clear();
    ui->lineEdit->clear();
}
