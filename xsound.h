#ifndef XSOUND_H
#define XSOUND_H

#include <QObject>
#include <Phonon/AudioOutput>
#include <Phonon/MediaObject>

class XSound : public Phonon::MediaObject
{
    Q_OBJECT
public:
    explicit XSound(QObject *parent = 0);
    ~XSound();
    void play(QString fileName);

private:
    Phonon::AudioOutput *audioOutput;
};

#endif // XSOUND_H
