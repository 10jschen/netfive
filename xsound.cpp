#include "xsound.h"

XSound::XSound(QObject *parent) :
    Phonon::MediaObject(parent)
{
    audioOutput = new Phonon::AudioOutput(Phonon::MusicCategory, parent);
}

void XSound::play(QString fileName)
{
    setCurrentSource(Phonon::MediaSource(fileName));
    Phonon::createPath(this, audioOutput);
    Phonon::MediaObject::play();
}

XSound::~XSound()
{
    delete audioOutput;
}
