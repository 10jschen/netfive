#-------------------------------------------------
#
# Project created by QtCreator 2012-07-09T15:17:04
#
#-------------------------------------------------

QT       += core gui network phonon

TARGET = NetFive
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    xsound.cpp

HEADERS  += mainwindow.h \
    xsound.h

FORMS    += mainwindow.ui

RESOURCES += \
    resource.qrc



